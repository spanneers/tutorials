import logging
from timeit import default_timer as timer
import grpc
import model_pb2
import model_pb2_grpc


def run_trainer():
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    logging.basicConfig()
    print("Calling FlairModel_Stub..")
    start_ch = timer()
    with grpc.insecure_channel('localhost:8061') as channel:
        stub = model_pb2_grpc.FlairModelStub(channel)
        ui_request = model_pb2.TrainingConfig(training_data_filename="train.txt",
                                              validation_data_filename="dev.txt",
                                              test_data_filename="test.txt",
                                              embeddings=[model_pb2.Embedding(embedding="wikipedia"),
                                                          model_pb2.Embedding(embedding="character")],
                                              epochs=1,
                                              learning_rate=0.1,
                                              batch_size=16,
                                              model_filename="test-model"
                                              )
        print("-------------- Start Training --------------")
        response = stub.startTraining(ui_request)

    print(response.status_text)
    end_ch = timer()
    print('Done!')
    ch_time = end_ch - start_ch
    print('Time for connecting to server = {}'.format(ch_time))

    return response.text


def run_predictor(data):
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    logging.basicConfig()
    print("Calling FlairModel_Stub..")
    start_ch = timer()
    with grpc.insecure_channel('localhost:8061') as channel:
        stub = model_pb2_grpc.FlairModelStub(channel)
        ui_request = model_pb2.Text(text=data)
        print("-------------- GetEntities --------------")
        response = stub.extract_entities_from_text(ui_request)

    end_ch = timer()
    print('Done!')
    ch_time = end_ch - start_ch
    print('Time for connecting to server = {}'.format(ch_time))

    return response.text


if __name__ == '__main__':
    logging.basicConfig()
    text = "Eine mysteriöse Lungenkrankheit ist in der zentralchinesischen Metropole Wuhan ausgebrochen. Bislang " \
           "seien 27 Erkrankte identifiziert worden, berichtete die Gesundheitskommission der Stadt am Dienstag. " \
           "Gerüchten im Internet, es könnte sich um einen neuen Ausbruch der Lungenseuche Sars handeln, " \
           "trat die «Volkszeitung» entgegen. Das Parteiorgan zitierte Experten, dass die Ursache gegenwärtig noch " \
           "unklar sei. Es könne jedoch nicht gefolgert werden, dass es sich um den Sars-Virus handele, schrieb das " \
           "Blatt. «Andere schwere Lungenentzündungen sind eher wahrscheinlich.» Die Gesundheitskommission " \
           "berichtete, viele der Infektionen könnten auf den Besuch des Huanan-Fischmarktes von Wuhan zurückgeführt " \
           "werden. Die Erkrankten seien in Quarantäne untergebracht worden. Sieben Patienten seien in einem ernsten " \
           "Zustand. Die anderen Fälle seien stabil. Zwei Patienten könnten in naher Zukunft entlassen werden. Die " \
           "Symptome seien vor allem Fieber. Wenige Patienten hätten Probleme mit der Atmung. Führende Experten seien " \
           "nach Wuhan gereist, um die Fälle genauer zu untersuchen, berichtete die Gesundheitskommission. Die Fälle " \
           "werden als virale Lungenentzündung behandelt. Eine Übertragung von Mensch zu Mensch oder eine Infektion " \
           "des medizinischen Personals sei noch nicht entdeckt worden, stellte die Behörde fest. Doch weckten die " \
           "Nachrichten aus Wuhan die Erinnerung an die Sars-Pandemie, die Ende 2002 begann. Das Severe Acute " \
           "Respiratory Syndrom (Sars) zählte zu den gefährlichsten Infektionswellen der jüngeren Zeit. Durch den " \
           "globalen Reiseverkehr erkrankten nach dem ersten Ausbruch in China weltweit mehr als 8000 Menschen - in " \
           "rund 30 Ländern und auf sechs Kontinenten. Wahrscheinlich sprang der Erreger in China von Tieren auf den " \
           "Menschen über und verbreitete sich über Husten und Niesen. Bei jedem zehnten Patienten war das Virus " \
           "tödlich. Nach offiziellen Angaben starben 774 Menschen an der schweren Atemwegserkrankung. "
    run_trainer()
