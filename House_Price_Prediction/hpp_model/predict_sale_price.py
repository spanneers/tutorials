import pandas as pd
import numpy as np
from sklearn.tree import DecisionTreeRegressor
from sklearn.preprocessing import MinMaxScaler


class HPP():
    def __init__(self, MSSubClass, LotArea, YearBuilt, TotRmsAbvGrd, GarageCars, GrLivArea, OverallQual) -> None:
        self.MSSubClass = MSSubClass
        self.LotArea = LotArea
        self.YearBuilt = YearBuilt
        self.TotRmsAbvGrd = TotRmsAbvGrd
        self.GarageCars = GarageCars
        self.GrLivArea = GrLivArea
        self.OverallQual = OverallQual
        self.data_preparation()
        self.prediction()

    def data_preparation(self):
        '''
        This function reads the train.csv dataset. We further pick the most important and relevant features and further normalize
        them to a standard scale.
        '''

        # Read the data and store in a dataframe called training_set
        train_data_path = 'train.csv'
        training_set = pd.read_csv(train_data_path)

        # Create a list of the predictor variables
        # Refer the READE.MD to understand the feature selection process
        self.predictors = [ "MSSubClass", "LotArea", "YearBuilt", "TotRmsAbvGrd", "GarageCars", "GrLivArea", "OverallQual"]

        # Select the dataframe with the necessary input features
        self.X = training_set[self.predictors]

        # Select the target variable and call it y
        self.y = training_set.SalePrice

        # Normalization of features in the dataset
        self.scaler_x = MinMaxScaler() 
        self.scaler_y = MinMaxScaler()

        # Scale the training input variables
        self.X_train_scaled = self.scaler_x.fit_transform(self.X)

        # Scale the training target variables
        y_train_array = self.y.values.reshape(-1, 1)
        self.y_train_scaled = self.scaler_y.fit_transform(y_train_array)
    
    def prediction(self):
        '''
        This function defines the model and predicts the required output for the inputs entered by the user in the web-UI.
        '''

        # Define the model
        tree_model = DecisionTreeRegressor(splitter='random', random_state=2)

        # Fit model 
        tree_model.fit(self.X_train_scaled, self.y_train_scaled)

        # Predict the SalePrice output with the inputs from the web-UI as entered by the user
        y_pred_UI = tree_model.predict(self.scaler_x.transform([[self.MSSubClass, self.LotArea, self.YearBuilt, self.TotRmsAbvGrd, 
                                          self.GarageCars, self.GrLivArea, self.OverallQual]]))
        
        # Rescale the predicted SalePrice in accordance to the standard scale
        y_pred_UI = self.scaler_y.inverse_transform(y_pred_UI.reshape(-1, 1))

        return y_pred_UI

    
def predict_sale_price(MSSubClass, LotArea, YearBuilt, TotRmsAbvGrd, GarageCars, GrLivArea, OverallQual):
    hpp = HPP(MSSubClass, LotArea, YearBuilt, TotRmsAbvGrd, GarageCars, GrLivArea, OverallQual)
    SalePrice_prediction = hpp.prediction()
    return SalePrice_prediction