# Tutorials

This repository contains the container specification and tutorials for Graphene and AI4EU Experiments 

https://www.ai4europe.eu/development

# Limitations of the current version
* protobuf import and enum semantics not yet supported, some standard structures need to be copied

# Container Specification and Tutorials
Tutorials and examples for the AI4EU Experiments docker/grpc format for models:
* The container Specification describes how the docker container for AI4EU Experiments should be configured
* The first simple example is a predictor for house prices
* The second slightly more complex example is a classifier for sentiments in movie reviews
* Please have a look at the AI4EU Experiments playlist: https://www.youtube.com/playlist?list=PLL80pOdPsmF6s6P6i2vZNoJ2G0cccwTPa
* And the platform Manual: https://github.com/ai4eu/tutorials/blob/master/Deliverable_AI4EU_D3.3_Platfrom_Manual.pdf
